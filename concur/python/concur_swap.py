from __future__ import print_function
from pprint import pprint
from threading import Thread
import threading
from random import randrange
from time import sleep

def make_lists(n_lists, items_per_list):
    return [list(range(i*items_per_list, i*items_per_list+items_per_list))
            for i in range(n_lists)]

def do_swap(lists, n_iters):
    n_lists = len(lists)
    n_items = len(lists[0])
    for _ in range(n_iters):
        list1 = randrange(n_lists)
        list2 = randrange(n_lists)
        idx1  = randrange(n_items)        
        idx2  = randrange(n_items)
        tmp = lists[list1][idx1]
        lists[list1][idx1] = lists[list2][idx2]
        lists[list2][idx2] = tmp

def count_unique(lists):
    items = set()
    for l in lists:
        items.update(l)
    return items

N_LISTS = 10
N_ITEMS = 10
N_THREADS = 100
N_ITERS   = 100
        
if __name__ == '__main__':
    lists = make_lists(N_LISTS, N_ITEMS)
    pprint(lists)
    print("Unique items: {}\n".format(len(count_unique(lists))))
    for i in range(N_THREADS):
        t = Thread(target=do_swap, args=[lists, N_ITERS])
        t.start()
    for t in threading.enumerate():
        if t is not threading.currentThread():
            t.join()
    pprint(lists)
    print("Unique items: {}".format(len(count_unique(lists))))
