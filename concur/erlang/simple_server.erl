- module(simple_server).
- compile(export_all).

loop(CountUnknown) ->
    receive
	{From, count} -> From ! {self(), CountUnknown},
			 loop(CountUnknown);
	{From, {reset, N}} -> From ! {self(), ack},
			      loop(N);
	{From, Msg} -> io:format("Server got ~w~n", [Msg]),
		       From ! {self(), ack},
		       loop(CountUnknown);
	terminate -> done;
	_ -> loop(CountUnknown + 1)
    end.

start() ->
    spawn(?MODULE, loop, [0]).

rpc(Pid, Msg) ->
    Pid ! {self(), Msg},
    receive
	{Pid, Response} -> Response
    end.
