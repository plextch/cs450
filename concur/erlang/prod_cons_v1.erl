-module(prod_cons_v1).
-export([start/0]).
-define(MAX_VAL, 10).

producer(Val, Consumer) ->
    if Val =:= ?MAX_VAL ->
            Consumer ! terminate;
       true ->
            Consumer ! Val,
            producer(Val + 1, Consumer)
    end.
            
consumer() ->
    timer:sleep(1000),
    receive
        terminate -> done;
        Val -> io:format("C: got ~w~n", [Val]),
               consumer()
    end.
             

start() ->
    C = spawn(fun consumer/0),
    spawn(fun() -> producer(0, C) end).
