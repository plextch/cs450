;;; atoms

(def count (atom 0))

(deref count)

@count

(swap! count inc)

(defn slow-inc [n]
  (Thread/sleep (rand-int 1000))
  ;; (println "Incrementing ( current val =" n ") in thread"
  ;;          (.getId (Thread/currentThread)))
  (+ n 1))

(defn multi-slow-inc [n-threads]
  (doall (apply pcalls ; "parallel calls"
                (repeat n-threads
                        #(swap! count slow-inc)))))