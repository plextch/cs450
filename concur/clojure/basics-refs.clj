;;; refs

(def a (ref 10))
(def b (ref 20))

(defn foo []
  (ref-set a 1))

(defn bar []
  (dosync
   (ref-set a 30)
   (alter b inc)))

(defn slow-swap [ref1 ref2]
  (dosync
   (let [val1 @ref1
         val2 @ref2]
     (Thread/sleep (rand-int 1000))
     ;; (println "Swapping" val1 "and" val2 "in thread"
     ;;          (.getId (Thread/currentThread)))
     (ref-set ref1 val2)
     (ref-set ref2 val1))))

(defn multi-slow-swap [n-threads]
  (dorun (apply pcalls
                (repeat n-threads
                        #(slow-swap a b)))))